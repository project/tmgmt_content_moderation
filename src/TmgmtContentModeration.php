<?php

namespace Drupal\tmgmt_content_moderation;

use Drupal\content_translation\ContentTranslationManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\content_moderation\ModerationInformationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\Context\Context;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\workflows\WorkflowInterface;

/**
 * Class TmgmtContentModeration.
 */
class TmgmtContentModeration implements TmgmtContentModerationInterface {

  /**
   * Drupal\Core\Entity\EntityRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Language\LanguageManagerInterface.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\content_moderation\ModerationInformationInterface definition.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $contentModerationInformation;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * TmgmtContentModeration constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\content_moderation\ModerationInformationInterface $content_moderation_information
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    ModerationInformationInterface $content_moderation_information,
    ConfigFactoryInterface $config_factory
   ) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->contentModerationInformation = $content_moderation_information;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}.
   */
  public function isSourceStateValidForTranslation(ContentEntityInterface $entity) {
    $config = $this->configFactory->get('tmgmt_content_moderation.settings');

    // If the moderation state filter is disabled, skip.
    if ((int) $config->get('enable_translation_by_state') !== 1) {
      return TRUE;
    }

    // If the entity type is not moderated, skip.
    if (!$this->contentModerationInformation->isModeratedEntityType($entity->getEntityType())) {
      return TRUE;
    }

    // Check if the translation source has the right state.
    $entityWorkflow = $this->contentModerationInformation->getWorkflowForEntity($entity);
    // @todo implement by revision type
    //   see 'source_state_revision_type' config.
    $entityModerationStateId = $this->getCurrentState($entity)->id();
    $validStates = $this->getTranslationStates($entityWorkflow);
    $validStateIds = array_map(function ($state) {
      return $state->id();
    }, $validStates);

    // We might need to check the available transitions here for the user
    // as TMGMT will expose the transition during the review.
    // https://www.drupal.org/project/tmgmt/issues/2978341
    return in_array($entityModerationStateId, $validStateIds);
  }

  /**
   * {@inheritdoc}.
   */
  public function getCurrentState(ContentEntityInterface $entity, $langcode = NULL, $revision_type = self::REVISION_TYPE_DEFAULT) {
    $result = NULL;
    // @todo implement language override.
    switch ($revision_type) {
      case TmgmtContentModerationInterface::REVISION_TYPE_LATEST:
        // @todo implement, see 'source_state_revision_type' config.
        break;

      case TmgmtContentModerationInterface::REVISION_TYPE_DEFAULT:
        $result = $this->contentModerationInformation->getOriginalState($entity);
        break;
    }
    return $result;
  }

  /**
   * {@inheritdoc}.
   */
  public function getDefaultRevisionStates(ContentEntityInterface $entity) {
    /** @var \Drupal\workflows\WorkflowInterface $workflow */
    $workflow = $this->contentModerationInformation->getWorkflowForEntity($entity);
    $workflowStates = $workflow->getTypePlugin()->getStates();
    $defaultRevisionStates = array_filter($workflowStates, function ($state) {
      return $state->isDefaultRevisionState();
    });
    return array_keys($defaultRevisionStates);
  }

  /**
   * {@inheritdoc}.
   */
  public function getTranslationStates(WorkflowInterface $workflow, $type = self::ENTITY_SOURCE) {
    $config = $this->configFactory->get('tmgmt_content_moderation.settings');
    $translatableWorkflowStates = $config->get('source_valid_states');
    if ($type === self::ENTITY_TRANSLATION) {
      $translatableWorkflowStates = $config->get('invalid_translation_states');
    }
    $result = [];
    if (is_array($translatableWorkflowStates)) {
      // Filter by content workflow.
      $validWorkflowStates = array_filter(
        $translatableWorkflowStates,
        function ($value, $key) use ($workflow) {
          return $key === $value && strpos($key, $workflow->id()) === 0;
        }, ARRAY_FILTER_USE_BOTH);

      $translatableStates = array_values(array_map(function ($value) use ($workflow) {
        return str_replace($workflow->id() . '/', '', $value);
      }, $validWorkflowStates));
      $workflowStates = $workflow->getTypePlugin()->getStates();
      $result = array_filter($workflowStates, function ($value) use ($translatableStates) {
        return in_array($value->id(), $translatableStates);
      });
    }
    return $result;
  }

  /**
   * {@inheritdoc}.
   */
  public function addContentModerationState(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->get('entity');
    $entityTranslationLanguages = $this->getModeratedTranslationLanguages($entity);
    $moderationStateByLanguage = [];
    foreach ($entityTranslationLanguages as $translationLanguage) {
      $langCode = $translationLanguage->getId();
      $dataType = 'language';
      $contextIdPrefix = '@language.current_language_context:';
      $contexts = [
        $contextIdPrefix . LanguageInterface::TYPE_CONTENT => new Context(new ContextDefinition($dataType), $langCode),
        $contextIdPrefix . LanguageInterface::TYPE_INTERFACE => new Context(new ContextDefinition($dataType), $langCode),
      ];
      $active = $this->entityRepository->getActive($entity->getEntityTypeId(), $entity->id(), $contexts);
      if ($active instanceof ContentEntityInterface) {
        $moderationStateByLanguage[$langCode] = $this->contentModerationInformation->getOriginalState($active)->label();
      }
    }

    // Get column indexes.
    $headerIndex = 0;
    foreach ($form['languages']['#header'] as $header) {
      $headerIndex++;
      if ($header instanceof TranslatableMarkup) {
        if ($header->getUntranslatedString() === 'Status') {
          $statusColumnIndex = $headerIndex;
        }
      }
    }

    // Modify links and operations with available providers.
    foreach ($form['languages']['#options'] as $langCode => &$languageRow) {
      $currentColumnIndex = 0;
      foreach ($languageRow as $rowKey => &$rowItem) {
        $currentColumnIndex++;

        // Add Moderation state if needed.
        if (
          $currentColumnIndex === $statusColumnIndex &&
          array_key_exists($langCode, $moderationStateByLanguage) &&
          array_key_exists('data', $rowItem)
        ) {
          $rowItem['data']['#template'] = $rowItem['data']['#template'] . ' [' . $moderationStateByLanguage[$langCode] . ']';
        }
      }
    }
  }

  /**
   * {@inheritdoc}.
   */
  public function getModeratedTranslationLanguages(ContentEntityInterface $entity) {
    $result = [];
    $languages = \Drupal::languageManager()->getLanguages();
    if (array_key_exists(LanguageInterface::LANGCODE_DEFAULT, $languages)) {
      unset($languages[LanguageInterface::LANGCODE_DEFAULT]);
    }
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $useLatestRevisions = $entity->getEntityType()->isRevisionable() && ContentTranslationManager::isPendingRevisionSupportEnabled($entity->getEntityTypeId(), $entity->bundle());
    $defaultRevision = $useLatestRevisions ? $storage->load($entity->id()) : $entity;
    foreach ($languages as $langCode => $language) {
      $latestRevisionId = $storage->getLatestTranslationAffectedRevisionId($defaultRevision->id(), $langCode);
      if ($latestRevisionId) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $latest_revision */
        $latestRevision = $storage->loadRevision($latestRevisionId);
        if (!$latestRevision->wasDefaultRevision() || $defaultRevision->hasTranslation($langCode)) {
          $result[$langCode] = $language;
        }
      }
    }
    return $result;
  }

}
