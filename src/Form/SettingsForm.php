<?php

namespace Drupal\tmgmt_content_moderation\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt_content_moderation\TmgmtContentModerationInterface;
use Drupal\workflows\Entity\Workflow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Render\RendererInterface $renderer
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tmgmt_content_moderation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tmgmt_content_moderation_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('tmgmt_content_moderation.settings');

    $form['content_moderation'] = [
      '#type' => 'details',
      '#title' => $this->t('Content Moderation'),
      '#open' => TRUE,
      '#access' => \Drupal::moduleHandler()->moduleExists('content_moderation'),
    ];
    $form['content_moderation']['enable_translation_by_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable translation by state'),
      '#description' => $this->t('Enable translation operations based on the moderation state of the entity. Uses the default revision. Affects the UI only (no constraint for routes yet).'),
      '#default_value' => $config->get('enable_translation_by_state'),
    ];

    // @todo make more fine grained configuration per workflow / entity type
    //   or move this as a third party setting for each state.
    $workflows = Workflow::loadMultiple();
    $moderationStates = [];
    /**
     * @var string $workflowId
     * @var \Drupal\workflows\Entity\Workflow $workflow
     */
    foreach ($workflows as $workflowId => $workflow) {
      foreach ($workflow->getTypePlugin()->getStates() as $stateId => $state) {
        $moderationStates[$workflowId . '/' . $stateId] = '<em>' . $workflow->label() . '</em> Workflow > ' . $state->label();
      }
    }
    $form['content_moderation']['source_valid_states'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Valid content moderation states for source'),
      '#options' => $moderationStates,
      '#description' => $this->t('If a source reaches the configured state(s), it will enable translation operations on the UI.'),
      '#default_value' => $config->get('source_valid_states') ?: [],
      '#states' => [
        'invisible' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Currently disabled, only default revision for now.
    $form['content_moderation']['source_state_revision_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Revision to use'),
      '#options' => [
        TmgmtContentModerationInterface::REVISION_TYPE_LATEST => $this->t('Latest'),
        TmgmtContentModerationInterface::REVISION_TYPE_DEFAULT => $this->t('Default'),
      ],
      '#description' => $this->t('Uses the default revision by default.'),
      '#default_value' => $config->get('source_state_revision_type') ?: TmgmtContentModerationInterface::REVISION_TYPE_DEFAULT,
      '#states' => [
        'invisible' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => TRUE],
        ],
      ],
      '#disabled' => TRUE,
    ];

    $form['content_moderation']['invalid_translation_states'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Invalid content moderation states for translations'),
      '#options' => $moderationStates,
      '#description' => $this->t('Limit the configured states to the source (exclude them from the entity translations). Example: "For translation" state.'),
      '#default_value' => $config->get('invalid_translation_states') ?: [],
      '#states' => [
        'invisible' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="enable_translation_by_state"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tmgmt_content_moderation.settings')
      ->set('enable_translation_by_state', $form_state->getValue('enable_translation_by_state'))
      ->set('source_valid_states', $form_state->getValue('source_valid_states'))
      ->set('source_state_revision_type', $form_state->getValue('source_state_revision_type'))
      ->set('invalid_translation_states', $form_state->getValue('invalid_translation_states'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
