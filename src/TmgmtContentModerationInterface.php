<?php

namespace Drupal\tmgmt_content_moderation;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\WorkflowInterface;

/**
 * Interface TmgmtContentModerationInterface.
 */
interface TmgmtContentModerationInterface {

  const REVISION_TYPE_LATEST = 'latest';

  const REVISION_TYPE_DEFAULT = 'default';

  const ENTITY_SOURCE = 'source';

  const ENTITY_TRANSLATION = 'translation';

  /**
   * Checks if an entity source moderation state is valid for translation.
   *
   * If no validation is required, return TRUE.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return bool
   */
  public function isSourceStateValidForTranslation(ContentEntityInterface $entity);

  /**
   * Returns the current entity translation moderation state.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface
   * @param string|null $langcode
   * @param string $revision_type
   *
   * @return \Drupal\content_moderation\ContentModerationState|null
   */
  public function getCurrentState(ContentEntityInterface $entity, $langcode = NULL, $revision_type = self::REVISION_TYPE_DEFAULT);

  /**
   * Get the default revision states for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return string[]
   */
  public function getDefaultRevisionStates(ContentEntityInterface $entity);

  /**
   * Returns the configured translation states.
   *
   * @param \Drupal\workflows\WorkflowInterface $workflow
   * @param string $type
   *
   * @return \Drupal\content_moderation\Entity\ContentModerationStateInterface[]
   */
  public function getTranslationStates(WorkflowInterface $workflow, $type = self::ENTITY_SOURCE);

  /**
   * Returns the list of translation languages for a content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getModeratedTranslationLanguages(ContentEntityInterface $entity);

  /**
   * Adds the Content Moderation state of the translations to the ContentTranslateForm.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function addContentModerationState(array &$form, FormStateInterface $form_state);

}
