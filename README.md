# Translation Management Content Moderation

Integrates TMGMT with Content Moderation.
WIP to test several Content Moderation features.

Relies on https://www.drupal.org/files/issues/2019-08-05/2978341_53.patch

## Initial features

- Display the current moderation state close to the published status
in the content translate form
- Enable translation operation only when the source content reaches
a specific state (example: “Published” state)
- Exclude states from the translated entity (example: “For translation” state)
- Redirect on Job completion to the latest revision of the entity

## Related issues

- [meta Translate unpublished node (revision) / Content moderation integration](https://www.drupal.org/project/tmgmt/issues/2942914)
- [Support pending revisions and accepting translation as a specific moderation state](https://www.drupal.org/project/tmgmt/issues/2978341)
